var app = new Vue({
	el: '#app',
	data: {
		user: "guest",
		loginMsj: "",
		loginBg: "",
		itemData: {
			"ip": "172.17.21.22",
			"serial": 123456
		}
	},
	methods: {
		getDatos() {
			$.get('/api/data')
				.done((data) => {
					console.log("refreshed")
					let json = data;
					app.user = json.user;

				}).fail((jqXHR, textStatus) => { alert("Failed: " + textStatus); });
		},
		login() {
			$.post("/api/login",
				{
					name: $("#email").val(),
					pwd: $("#pwd").val()
				})
				.done(() => {
					app.getDatos();
					this.user = $("#email").val();
					$("#email").val("");
					$("#pwd").val("");
					app.showMsj("Login Successful!", "bg-success");
				})
				.fail(() => {
					app.showMsj("Wrong user or password", "bg-danger");
					$("#pwd").val("");
				})
		},
		logout() {
			$.post("/api/logout")
				.done(() => {
					app.showMsj("Logout Successful!", "bg-success");
					app.getDatos();
				})
				.fail(() => { app.showMsj("Logout fail", "bg-danger"); })
		},
		register() {
			if (app.ValidateEmail($("#email").val()) && $("#pwd").val().length > 4) {
				$.post("/api/register", { userName: $("#email").val(), password: $("#pwd").val() })
					.done(() => {
						app.showMsj("User registered successfully!!", "bg-success");
					})
					.fail((response) => { app.showMsj(response.responseText, "bg-warning"); });
			}
			else {
				app.showMsj("invalid email or password", "bg-danger");
			}
		},
		showMsj(msj, bgClass) {
			app.loginMsj = msj;
			app.loginBg = bgClass;
			$('#loginInfo').hide().show("slow").delay(2000).fadeOut("slow");
		},
	},
	created() {
		this.getDatos();
	}
})