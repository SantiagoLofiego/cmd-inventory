var app = new Vue({

  el: '#app',
  data: {
    user: "guest",
    games: [],
    leaderBoard: [],
    loginMsj: "",
    loginBg: ""
  },
  methods: {
    getDatos() {
      $.get('/api/games')
        .done((data) => {
          console.log("refreshed")
          let json = data;
          app.user = json.player;
          app.games = json.games;
          app.games.forEach(game => {
            game.gamePlayers.sort((a,b) => (a.id > b.id)? 1:-1);
          })
          app.leaderBoard = json.leaderBoard;
        }).fail((jqXHR, textStatus) => { alert("Failed: " + textStatus);});
    },
    login() {
      $.post("/api/login",
        {
          name: $("#email").val(),
          pwd: $("#pwd").val()
        })
        .done(() => {
          $("#email").val("");
          $("#pwd").val("");
          app.getDatos();
          app.showMsj("Login Successful!", "bg-success");
        })
        .fail(() => {
          app.showMsj("Wrong user or password", "bg-danger");
          $("#pwd").val("");
        })
    },
    logout() {
      $.post("/api/logout")
        .done(() => {
          app.getDatos();
          app.showMsj("Logout Successful!", "bg-success");
        })
        .fail(() => { app.showMsj("Logout fail", "bg-danger"); })
    },
    register() {
      if (app.ValidateEmail($("#email").val()) && $("#pwd").val().length > 4) {
        $.post("/api/register", { userName: $("#email").val(), password: $("#pwd").val() })
          .done(() => {
            app.showMsj("User registered successfully!!", "bg-success");
          })
          .fail((response) => { app.showMsj(response.responseText, "bg-warning"); });
      }
      else {
        app.showMsj("invalid email or password", "bg-danger");
      }
    },
    newGame() {
      $.post("/api/games").done((data) => {
        let url = "/web/game.html?Gp=" + data.gpid;
        app.showMsj("Game Created!! Redirecting...", "bg-success");
        setTimeout(() => location.href = url, 2500);
      })

    },
    joinGame(gameId) {
      $.post('/api/game/' + gameId + '/players')
        .done((data) => {
          let url = "/web/game.html?Gp=" + data.gpid;
          app.showMsj("Successfully joined!! Redirecting...", "bg-success");
          setTimeout(() => location.href = url, 2500);
        })
        .fail((error) => { app.showMsj("You must be logged to join a game", "bg-danger"); })
    },
    showButton(game) {
      let gamePlayerIsUser = false;
      game.gamePlayers.forEach(gamePlayer => {
        if (gamePlayer.player.email == app.user.email) {
          gamePlayerIsUser = true;
        }
      });
      if (gamePlayerIsUser) {
        return true;
      } else if (game.gamePlayers.length < 2) {
        return true
      }
      return false;
    },
    getBtnMsj(game) {
      let gamePlayerIsUser = false;
      game.gamePlayers.forEach(gamePlayer => {
        if (gamePlayer.player.email == app.user.email) {
          gamePlayerIsUser = true;
        }
      });
      if (gamePlayerIsUser) {
        return "Enter";
      } else if (game.gamePlayers.length < 2) {
        return "Join Game"
      }
    },
    gameBtnEvnt(game) {
      let gpid;
      let gamePlayerIsUser = false;
      game.gamePlayers.forEach(gamePlayer => {
        if (gamePlayer.player.email == app.user.email) {
          gamePlayerIsUser = true;
          gpid = gamePlayer.id;
        }
      });
      if (gamePlayerIsUser) {
        location.href = "/web/game.html?Gp=" + gpid;
      } else if (game.gamePlayers.length < 2) {
        this.joinGame(game.id);
      }
    },
    localDate(date) {
      return new Date(date).toLocaleString();
    },
    ValidateEmail(mail) {
      let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (mail.match(mailformat)) {
        return (true)
      }
      //alert("You have entered an invalid email address!")
      return (false)
    },
    showMsj(msj, bgClass) {
      app.loginMsj = msj;
      app.loginBg = bgClass;
      $('#loginInfo').hide().show("slow").delay(2000).fadeOut("slow");
    },
    refresh(){
      setInterval(()=>app.getDatos(),10000);
    }
  },
  computed: {
    leaderBoardOrdered() {
      let list = this.leaderBoard.sort((a, b) => { return b.scores.total - a.scores.total; });
      return list.slice(0, 10);
    },
  },
  created() {
    this.getDatos();
    this.refresh();
  }
})
