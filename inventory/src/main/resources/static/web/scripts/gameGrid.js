//Estas son las opciones para la biblioteca gridstack
var options = {
  width: 10, //cantidad maxima de columnas
  height: 10, //cantidad maxima de filas
  verticalMargin: 0,//mas claro imposible
  cellHeight: 45, //altura de las celdas. Por dafecto la altura de la grid es dinámica. El ancho de las celdas es determinado por la cantidad repartida en el tamaño de la grid
  disableResize: true,//Si es verdadero no es posible modificar el tamaño de los Widgets mediante la interfáz grafica
  disableOneColumnMode: true,//desabilita el modo de una columna si el ancho de la ventana es menor al ancho minimo de la grid 
  resizable: {
    disabled: true, 
    handles: 'e' 
  },
  animate: false,
  float: true,
  resizable: false
}

$('.grid-stack').gridstack(options);
var grid = $('#grid').data('gridstack');
var boardGrid= $('#gridBoard').data('gridstack');
boardGrid.setStatic(true);
var opGridBoard= $('#gridBoard').data('gridstack');
opGridBoard.setStatic(true);
grid.resizable('.grid-stack-item', false);
var widgets = $('#grid > .grid-stack-item:visible');
var shipBoard = getWidgetsData(widgets);
$('div.ui-resizable-handle').hide();//disable resizable handlers

$("#grid").on("dragstart", function (event, ui) {
  console.log("drag-Start")
})

$("#grid").on("dragstop", function (event, ui) {
  console.log(["dragstop",event.target]);
  let ship = ($(event.target).data('_gridstack_node'))
  app.selectedShip = app.getShip(ship);
  app.selectedShip.type = event.target.id;
  relocateBtn(ship);
})

$('#grid').on('change', function (event, items) {
  widgets = $('#grid > .grid-stack-item:visible');
});

function getWidgetsData(widgetCollection) {
  let widgetsData = []
  for (widget of widgetCollection) {
    let node = $(widget).data('_gridstack_node');
    console.log()
    widgetsData.push({
      id: widget.id,
      x: node.x,
      y: node.y,
      width: node.width,
      height: node.height
    });
  }
  console.log(widgetsData)
  return widgetsData;
}

function relocateBtn(shipNode) {
  if (shipNode.x == 0) { $('.rotateBtn').animate({ left: '45px' }, 100) }
  else { $('.rotateBtn').animate({ left: '-45px' }, 100) }
  if (shipNode.y == 0) { $('.rotateBtn').animate({ top: '45px' }, 100) }
  else { $('.rotateBtn').animate({ top: '-45px' }, 100) }
}