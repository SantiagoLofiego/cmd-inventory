package com.cmd.inventory.controllers;

import com.cmd.inventory.models.Person;
import com.cmd.inventory.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.cmd.inventory.util.Util.isGuest;

@RestController
@RequestMapping("/api")
public class AppController {
    @Autowired
    private PersonRepository personRepository;

    @RequestMapping("/data")
    public ResponseEntity getData(Authentication authentication) {
        Map<String,Object>dto = new LinkedHashMap<>();
        if (isGuest(authentication)) {
            dto.put("user","guest");
        }else {
            Person user = personRepository.findByEmail(authentication.getName());
            dto.put("user", user.makePersonDTO());
        }
        return new ResponseEntity(dto, HttpStatus.OK);
    }
}
