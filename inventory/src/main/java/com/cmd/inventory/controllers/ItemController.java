package com.cmd.inventory.controllers;

import com.cmd.inventory.models.Field;
import com.cmd.inventory.models.Item;
import com.cmd.inventory.repositories.FieldRepository;
import com.cmd.inventory.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class ItemController {

    @Autowired
    ItemRepository itemRepository;
    @Autowired
    FieldRepository fieldRepository;

    @RequestMapping(value = "/items/update/{itemId}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateItem (@PathVariable Long itemId, Authentication authentication, @RequestBody Map<String,Object> fields){
        Item item = itemRepository.findById(itemId).orElse(null);
        
        fields.forEach((k,v)->{
            Field field = fieldRepository.findByKey(k);
            String typeOfValue = field.getTypeOfValue();
            if (typeOfValue.equals("String")) {
                item.getStringFields().put(k,v.toString());
            }else if(typeOfValue.equals("Long")){
                item.getLongFields().put(k, (Long.valueOf(v.toString())));
            }else if (typeOfValue.equals("Boolean")){
                item.getBooleanFields().put(k, ((boolean) v));
            }
        });

        itemRepository.save(item);
        return new ResponseEntity<>("ok", HttpStatus.OK);

    }
}
