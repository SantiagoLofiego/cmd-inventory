package com.cmd.inventory.models;

        import org.hibernate.annotations.GenericGenerator;

        import javax.persistence.*;
        import java.util.LinkedHashMap;
        import java.util.List;
        import java.util.Map;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @ManyToOne(fetch  = FetchType.EAGER)
    @JoinColumn(name="typeID")
    private Type type;

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY)
    private List<Assignment>assignments;

    @ElementCollection
    @Column(name = "longFields")
    private Map<String,Long > longFields= new LinkedHashMap<>();

    @ElementCollection
    @Column(name = "stringFields")
    private Map<String,String > stringFields =new LinkedHashMap<>();

    @ElementCollection
    @Column(name = "booleanFields")
    private Map<String, Boolean> booleanFields = new LinkedHashMap<>();
    //constructors

    public Item() {
    }

    public Item(Type type) {
        this.type = type;
        type.getCustomFields().forEach(field ->{
            if (field.getTypeOfValue().equals("Long")) {
                this.longFields.put(field.getKey(), Long.valueOf(field.getDefaultValue()));
            }else if (field.getTypeOfValue().equals("String")){
                this.stringFields.put(field.getKey(),field.getDefaultValue());
            }else this.booleanFields.put(field.getKey(),Boolean.valueOf(field.getDefaultValue()));
        });



    }

    //getters
    public long getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public Map<String, Long> getLongFields() {
        return longFields;
    }

    public Map<String, String> getStringFields() {
        return stringFields;
    }

    public Map<String, Boolean> getBooleanFields() {
        return booleanFields;
    }



    //setters

    public void setType(Type type) {
        this.type = type;
    }


}
