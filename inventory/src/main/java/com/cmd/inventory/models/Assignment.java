package com.cmd.inventory.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Assignment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private Date creationDate;

    private Date finishDate;

    private Date returnedDate;

    private boolean returned;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userID")
    private Person user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "itemID")
    private Item item;

    //Constructors

    public Assignment() {
    }

    public Assignment(Date finishDate, Person user, Item item) {
        this.finishDate = finishDate;
        this.user = user;
        this.item = item;
        this.returned = false;
        this.creationDate= new Date();

    }
    //Getters

    public long getId() {
        return id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public Date getReturnedDate() {
        return returnedDate;
    }

    public boolean isReturned() {
        return returned;
    }

    public Person getUser() {
        return user;
    }

    public Item getItem() {
        return item;
    }

    //Setters

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public void setReturnedDate(Date returnedDate) {
        this.returnedDate = returnedDate;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public void setUser(Person user) {
        this.user = user;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
