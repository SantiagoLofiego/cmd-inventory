package com.cmd.inventory.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @OneToMany(mappedBy = "type", fetch = FetchType.EAGER)
    private Set<Item> item;

    private String name;

    @OneToMany(mappedBy = "type", fetch = FetchType.EAGER)
    private List<Field> customFields;

    //constructors
    public Type (){

    }

    public Type(String name, List<Field> customFields) {
        this.name = name;
        this.customFields = customFields;
    }

    //getters


    public long getId() {
        return id;
    }

    public Set<Item> getItem() {
        return item;
    }

    public String getName() {
        return name;
    }

    public List<Field> getCustomFields() {
        return customFields;
    }

    //setters


    public void setItem(Set<Item> item) {
        this.item = item;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCustomFields(List<Field> customFields) {
        this.customFields = customFields;
    }
}
