package com.cmd.inventory.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Field {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    @ManyToOne(fetch  = FetchType.EAGER)
    @JoinColumn(name="typeID")
    private Type type;

    private String typeOfValue;

    private String key;

    private String defaultValue;

    //constructors

    public Field(){}

    public Field (String name,String typeOfValue,String defaultValue){
        this.key= name;
        this.typeOfValue = typeOfValue;
        this.defaultValue = defaultValue;
    }

    //getters

    public long getId() {
        return id;
    }

    public String getTypeOfValue() {
        return typeOfValue;
    }

    public String getKey() {
        return key;
    }

    public Type getType() {
        return type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    //setters


    public void setType(Type type) {
        this.type = type;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public void setTypeOfValue(String typeOfValue) {
        this.typeOfValue = typeOfValue;
    }

    public void setKey(String key) {
        this.key = key;
    }


}
