package com.cmd.inventory.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Member {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native",strategy = "native")
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "personID")
    private Person person;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "groupID")
    private Team group;

    //Constructors

    public Member() {
    }

    public Member(Person person, Team group) {
        this.person = person;
        this.group = group;
    }
    //Getters

    public long getId() {
        return id;
    }

    public Person getPerson() {
        return person;
    }

    public Team getGroup() {
        return group;
    }

    //Setters

    public void setPerson(Person person) {
        this.person = person;
    }

    public void setGroup(Team group) {
        this.group = group;
    }
}
