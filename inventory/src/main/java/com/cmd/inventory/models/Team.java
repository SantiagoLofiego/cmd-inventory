package com.cmd.inventory.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;

    private String name;

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    private Set<Member> members;

    //Constructors

    public Team() {
    }

    public Team(String name) {
        this.name = name;
        this.members = new HashSet<>();
    }

    //Getters

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Member> getMembers() {
        return members;
    }


    //Setters

    public void setName(String name) {
        this.name = name;
    }
}
