package com.cmd.inventory;

import com.cmd.inventory.models.*;
import com.cmd.inventory.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.InetAddress;
import java.util.Arrays;

@SpringBootApplication
public class InventoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryApplication.class, args);


	}

	@Bean
	public PasswordEncoder encoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}
	@Bean
	public CommandLineRunner initData(PersonRepository personRepository,
									  TypeRepository typeRepository,
									  ItemRepository itemRepository,
									  FieldRepository fieldRepository,
									  TeamRepository groupRepository,
									  MemberRepository memberRepository) {
		return (args) -> {
			Person user1 = new Person("santiagolofiego@gmail.com",encoder().encode("123456"),"ADMIN");
			Person user2 = new Person("santiagolofiego@yahoo.com",encoder().encode("123456"),"USER");
			user1.setName("Santiago");
			user1.setLastName("Lofiego");
			user2.setName("Santiago");
			user2.setLastName("Lofiego");
			personRepository.save(user1);
			personRepository.save(user2);
			System.out.println("FINISH LOADING");

			Team grupo1 = new Team("Sistemas");
			groupRepository.save(grupo1);

			Member member1 = new Member(user1,grupo1);
			memberRepository.save(member1);

			Field campo1 = new Field("ip", "String","");
			Field campo2 = new Field("serial","Long","0");
			Field campo3 = new Field("active","Boolean","true");
			Field campo4 = new Field("vendor","String","");
			fieldRepository.save(campo1);
			fieldRepository.save(campo2);
			fieldRepository.save(campo3);
			fieldRepository.save(campo4);
			Type computer = new Type("computer", Arrays.asList(campo1,campo2,campo3,campo4));
			typeRepository.save(computer);

			Item computer1 = new Item(computer);


			itemRepository.save(computer1);



			InetAddress ip = InetAddress.getByName("172.17.21.20");
			System.out.println(ip.getHostAddress());
			Field prueba =fieldRepository.findByKey("serial");


		};
	}
}

@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

	@Autowired
	PersonRepository personRepository;

	@Override
	public void init(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(inputName -> {
			Person user = personRepository.findByEmail(inputName);
			if (user != null) {
				return new User(user.getEmail(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRole()));
			} else {
				System.out.println("no existe ese usuario :(");
				throw new UsernameNotFoundException("Unknown user: " + inputName);
			}
		});
	}

}

@Configuration
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/rest/**").permitAll()
				.antMatchers("/web/user.html").hasAnyAuthority("USER","ADMIN")
				.antMatchers("/web/admin.html*").hasAuthority("ADMIN")
				.antMatchers("/h2-console/**").permitAll()
				.antMatchers("/api/data/**").permitAll();

		http.formLogin()
				.usernameParameter("name")
				.passwordParameter("pwd")
				.loginPage("/api/login");

		http.logout().logoutUrl("/api/logout");

		// turn off checking for CSRF tokens
		http.csrf().disable();

		// if user is not authenticated, just send an authentication failure response
		http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if login is successful, just clear the flags asking for authentication
		http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

		// if login fails, just send an authentication failure response
		http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

		// if logout is successful, just send a success response
		http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
		//to access h2 console with security active
		http.headers().frameOptions().disable();
	}

	private void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		}
	}
}


