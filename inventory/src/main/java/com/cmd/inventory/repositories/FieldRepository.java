package com.cmd.inventory.repositories;

import com.cmd.inventory.models.Field;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface FieldRepository extends JpaRepository<Field,Long> {
    Field findByKey(String fieldKey);
}
