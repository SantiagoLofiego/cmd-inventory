package com.cmd.inventory.repositories;

import com.cmd.inventory.models.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AssignmentRepository extends JpaRepository<Assignment,Long> {
}
